from django.conf.urls  import url,include
from django.urls import path
from rest_framework.routers import DefaultRouter
from ran.views import UserDetailsViewSet
from ran import views
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf import settings
from django.conf.urls.static import static
router=DefaultRouter()
router.register(r'ran',views.UserDetailsViewSet)
user_list = UserDetailsViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

user_detail = UserDetailsViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
urlpatterns = format_suffix_patterns([
    path('ran/', user_list, name='user-list'),
    path('ran/<int:pk>/', user_detail, name='user-detail'),
    ])

urlpatterns=[
    path('',include(router.urls)),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)